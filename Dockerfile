FROM	ubuntu:bionic

MAINTAINER	morph027 "morphsen@gmx.com"

RUN	(echo "keyboard-configuration keyboard-configuration/layout select German" | debconf-set-selections) \
	&& (echo "keyboard-configuration keyboard-configuration/layoutcode string de" | debconf-set-selections)

RUN	apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get -y -qq install \
		autoconf-archive \
		cdbs \
		cinnamon-settings-daemon-dev \
		dbus-x11 \
		devscripts \
		dh-exec \
		dh-python \
		dpkg-dev \
		gir1.2-meta-muffin-0.0 \
		git-core \
		gnome-bluetooth \
		gnome-common \
		gnome-doc-utils \
		gnome-pkg-tools \
		gobject-introspection \
		gtk-doc-tools \
		gvfs-backends \
		libaccountsservice-dev \
		libcaribou-dev \
		libcinnamon-desktop-dev \
		libcjs-dev \
		libclutter-gst-3.0-dev \
		libclutter-gtk-1.0-dev \
		libcolord-dev \
		libcroco3-dev \
		libcryptui-dev \
		libcups2-dev \
		libcvc3-dev \
		libdjvulibre-dev \
		libevince-dev \
		libexempi-dev \
		libexif-dev \
		libgail-3-dev \
		libgcr-3-dev \
		libgcrypt20-dev \
		libgirepository1.0-dev \
		libgnome-bluetooth-dev \
		libgnome-keyring-dev \
		libgnomekbd-dev \
		libgoa-1.0-dev \
		libgoa-backend-1.0-dev \
		libgpgme11-dev \
		libgtksourceview-3.0-dev \
		libgudev-1.0-dev \
		libgxps-dev \
		libkpathsea-dev \
		liblcms2-dev \
		libmhash-dev \
		libmm-glib-dev \
		libmozjs-52-dev \
		libmusicbrainz5-dev \
		libnemo-extension-dev \
		libnma-dev \
		libnss3-dev \
		libpam0g-dev \
		libpangox-1.0-0 \
		libpangox-1.0-dev \
		libpolkit-agent-1-dev \
		libpoppler-glib-dev \
		libpulse-dev \
		libreadline-dev \
		librsvg2-bin \
		librsvg2-dev \
		libsecret-1-dev \
		libsoup2.4-dev \
		libspectre-dev \
		libsystemd-dev \
		libupower-glib-dev \
		libwebkit2gtk-4.0-dev \
		libwnck-dev \
		libxcb-res0-dev \
		libxkbcommon-x11-dev \
		libxkbfile-dev \
		libxss-dev \
		libxt-dev \
		locales \
		mate-common \
		meson \
		optipng \
		python-all \
		python-dev \
		python-distutils-extra \
		python-docutils \
		python-gi-dev \
		python-gtk2 \
		python3-all \
		python3-dev \
		python3-polib \
		python3-setuptools \
		ruby-sass \
		uuid-runtime \
		valac \
		xmlto \
		xserver-xorg-input-wacom \
		xvfb \
		yelp-tools \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*
