# Cinnamon Packages

Built packages following [instructions](http://segfault.linuxmint.com/2013/10/how-to-build-and-install-the-latest-cinnamon-from-git/) by Clément Lefebvre.

## Add this repo

### Add repo signing key to apt

```bash
wget -q -O - https://packaging.gitlab.io/cinnamon/gpg.key | sudo apt-key add -
```

or

```bash
curl -sL https://packaging.gitlab.io/cinnamon/gpg.key | sudo apt-key add - 
```

### Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/cinnamon bionic main" | sudo tee /etc/apt/sources.list.d/morph027-cinnamon.list
```


## Build yourself

All the work is done inside script ```.gitlab-ci/package.sh```.

I recommend to use Docker for building (does not mess up and litter your system with packages).

### Docker

Docker images based on Ubuntu Xenial.

```bash
$ mkdir workdir
$ docker run --rm -it -v $PWD/workdir:/workdir -w /workdir registry.gitlab.com/packaging/cinnamon:bionic /bin/bash
$ .gitlab-ci/package.sh
```

If you want to build for newer Ubuntu versions, just fetch the Dockerfile, edit the ```FROM``` directive and build the image.

```bash
$ export MY_UBUNTU=cosmic
$ mkdir -p workdir/docker
$ cd workdir/docker
$ curl -LO https://gitlab.com/packaging/cinnamon/raw/master/Dockerfile
$ sed -i 's,ubuntu:bionic,ubuntu:'$MY_UBUNTU',' Dockerfile
$ docker build -t cinnamon-builder:$MY_UBUNTU .
```

Now, just replace the Docker run command with:

```bash
$ mkdir workdir
$ docker run --rm -it -v $PWD/workdir:/workdir -w /workdir cinnamon-builder:$MY_UBUNTU /bin/bash
$ .gitlab-ci/package.sh
```
