#!/bin/bash

apt-get update

components=()
components+=( "cinnamon-menus" )
components+=( "xreader" )
components+=( "xapps" )
components+=( "flags" )
components+=( "cjs" )
components+=( "cinnamon-desktop" )
components+=( "cinnamon-translations" )
components+=( "cinnamon-session" )
components+=( "cinnamon-settings-daemon" )
components+=( "cinnamon-control-center" )
components+=( "muffin" )
components+=( "Cinnamon" )
components+=( "nemo" )
components+=( "nemo-extensions" )
components+=( "folder-color-switcher" )
components+=( "cinnamon-screensaver" )
components+=( "cinnamon-themes" )
components+=( "mint-y-theme" )
components+=( "mint-x-icons" )

base_dir=$PWD

for component in ${components[@]}
do
  if [ ! -d ${base_dir}/${component}/${component} ]; then
    mkdir ${base_dir}/${component}
    git clone https://github.com/linuxmint/${component}.git ${base_dir}/${component}/${component}
  fi
  cd ${base_dir}/${component}/${component}
  case "$component" in
    *)
      latesttag=$(git describe --tags)
      git checkout ${latesttag}
    ;;
  esac

  case "$component" in
    "nemo-extensions")
      ./buildall 2>&1 | tee ${base_dir}/dpkg-buildpackage-${component}.log
    ;;
    *)
      apt-get -y build-dep ${component}
      dpkg-buildpackage 2>&1 | tee ${base_dir}/dpkg-buildpackage-${component}.log
      (dpkg -i ${base_dir}/${component}/*deb || apt-get -f install -y) | tee ${base_dir}/install-${component}.log
    ;;
  esac
done

find ${base_dir} -type f -name '*.deb' -exec mv {} ${base_dir} \;
